﻿using System.Text.Json.Serialization;
using Entities.Enums;

namespace Services
{
    public class InputEmployee
    {
        [JsonPropertyName("name")]
        public string Name { get; set; }

        [JsonPropertyName("age")]
        public int Age { get; set; }

        [JsonPropertyName("genderType")]
        public GenderType GenderType { get; set; }

        [JsonPropertyName("phone")]
        public string Phone { get; set; }
    }
}
