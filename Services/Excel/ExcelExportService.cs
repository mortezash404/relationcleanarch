﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Entities;
using Npoi.Mapper;

namespace Services.Excel
{
    public class ExcelExportService : IExcelExportService
    {
        public async Task Export(Employee employee)
        {
            var mapper = new Mapper();

            var data = new List<Employee>
            {
                employee
            };

            await Task.Factory.StartNew(() =>
            {
                mapper.Save<Employee>(@"C:\Users\user\Desktop\employee.xlsx", data, "sheet1", false, true);
            });
        }
    }
}
