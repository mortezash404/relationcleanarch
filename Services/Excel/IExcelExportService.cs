﻿using System.Threading.Tasks;
using Entities;

namespace Services.Excel
{
    public interface IExcelExportService
    {
        Task Export(Employee employee);
    }
}