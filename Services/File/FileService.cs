﻿using System.IO;
using System.Text.Json;
using System.Threading.Tasks;
using Entities;
using Microsoft.Extensions.Options;

namespace Services.File
{
    public class FileService : IFileService
    {
        private readonly FileSetting _options;

        public FileService(IOptions<FileSetting> options)
        {
            _options = options.Value;
        }

        public async Task<Employee> GetContentAsync(string fileName)
        {
            var path = Path.Combine(_options.Address, fileName);

            var text = System.IO.File.ReadAllText(path);

            var inputEmployee = JsonSerializer.Deserialize<InputEmployee>(text);

            var employee = new Employee
            {
                Name = inputEmployee.Name,
                Age = inputEmployee.Age,
                GenderType = inputEmployee.GenderType,
                Phone = inputEmployee.Phone
            };

            // convert return type to awaitable (async)

            return  await Task.FromResult<Employee>(employee);
        }
    }
}
