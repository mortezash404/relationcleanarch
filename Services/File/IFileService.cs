﻿using System.Threading.Tasks;
using Entities;

namespace Services.File
{
    public interface IFileService
    {
        Task<Employee> GetContentAsync(string fileName);
    }
}