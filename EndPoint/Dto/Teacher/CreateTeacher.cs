﻿using Entities;
using Entities.Enums;

namespace EndPoint.Dto.Teacher
{
    public class CreateTeacher
    {
        public string Name { get; set; }

        public DegreeType DegreeType { get; set; }

        public int Age { get; set; }
    }
}
