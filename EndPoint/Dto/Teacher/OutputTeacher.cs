﻿using System.Collections.Generic;
using Entities.Enums;

namespace EndPoint.Dto.Teacher
{
    public class OutputTeacher
    {
        public string Name { get; set; }

        public DegreeType DegreeType { get; set; }

        public int Age { get; set; }

        // navigation property

        public ICollection<Entities.Course> Courses { get; set; }
    }
}
