﻿using AutoMapper;

namespace EndPoint.Dto.Teacher
{
    public class TeacherProfile : Profile
    {
        public TeacherProfile()
        {
            CreateMap<CreateTeacher,Entities.Teacher>();
            CreateMap<Entities.Teacher,OutputTeacher>();
        }
    }
}
