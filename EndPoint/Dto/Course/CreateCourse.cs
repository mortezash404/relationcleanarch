﻿namespace EndPoint.Dto.Course
{
    public class CreateCourse
    {
        public string Name { get; set; }

        public int Unit { get; set; }

        public int? TeacherId { get; set; }
    }
}
