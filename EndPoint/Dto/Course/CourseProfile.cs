﻿using AutoMapper;

namespace EndPoint.Dto.Course
{
    public class CourseProfile : Profile
    {
        public CourseProfile()
        {
            CreateMap<CreateCourse,Entities.Course>();
            CreateMap<Entities.Course,OutputCourse>();
        }
    }
}
