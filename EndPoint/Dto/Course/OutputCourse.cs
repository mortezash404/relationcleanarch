﻿namespace EndPoint.Dto.Course
{
    public class OutputCourse
    {
        public string Name { get; set; }

        public int Unit { get; set; }
    }
}
