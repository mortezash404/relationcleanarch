﻿using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using DAL.Repositories.Contracts;
using DAL.UnitOfWork;
using EndPoint.Dto.Course;
using Entities;

namespace EndPoint.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CoursesController : ControllerBase
    {
        #region Dependency Injection

        private readonly ICourseRepository _repository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        #endregion
        
        #region Constructor
        public CoursesController(ICourseRepository repository, IUnitOfWork unitOfWork, IMapper mapper)
        {
            _repository = repository;
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        } 
        #endregion

        // GET: api/Courses
        //[HttpGet]
        //public async Task<ActionResult<IEnumerable<Course>>> GetCourses()
        //{
        //    return await _context.Courses.ToListAsync();
        //}

        // GET: api/Courses/5
        [HttpGet("{id}")]
        public async Task<ActionResult<OutputCourse>> GetCourse(int id)
        {
            var course = await _repository.GetByIdAsync(id);

            if (course == null)
            {
                return NotFound();
            }

            return _mapper.Map<OutputCourse>(course);
        }

        // PUT: api/Courses/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        //[HttpPut("{id}")]
        //public async Task<IActionResult> PutCourse(int id, Course course)
        //{
        //    if (id != course.Id)
        //    {
        //        return BadRequest();
        //    }

        //    _context.Entry(course).State = EntityState.Modified;

        //    try
        //    {
        //        await _context.SaveChangesAsync();
        //    }
        //    catch (DbUpdateConcurrencyException)
        //    {
        //        if (!CourseExists(id))
        //        {
        //            return NotFound();
        //        }
        //        else
        //        {
        //            throw;
        //        }
        //    }

        //    return NoContent();
        //}

        // POST: api/Courses
        
        [HttpPost]
        public async Task<ActionResult<OutputCourse>> PostCourse(CreateCourse input)
        {
            var course = _mapper.Map<Course>(input);

            await _repository.InsertAsync(course);

            await _unitOfWork.CompleteAsync();

            return Ok(_mapper.Map<OutputCourse>(course));
        }

        // DELETE: api/Courses/5
        //[HttpDelete("{id}")]
        //public async Task<ActionResult<Course>> DeleteCourse(int id)
        //{
        //    var course = await _context.Courses.FindAsync(id);
        //    if (course == null)
        //    {
        //        return NotFound();
        //    }

        //    _context.Courses.Remove(course);
        //    await _context.SaveChangesAsync();

        //    return course;
        //}

        //private bool CourseExists(int id)
        //{
        //    return _context.Courses.Any(e => e.Id == id);
        //}
    }
}
