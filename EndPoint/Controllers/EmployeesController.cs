﻿using System.Threading.Tasks;
using DAL.Repositories.Contracts;
using DAL.UnitOfWork;
using Microsoft.AspNetCore.Mvc;
using Services.Excel;
using Services.File;

namespace EndPoint.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmployeesController : ControllerBase
    {
        private readonly IExcelExportService _excelExportService;
        private readonly IFileService _fileService;
        private readonly IEmployeeRepository _repository;
        private readonly IUnitOfWork _unitOfWork;

        public EmployeesController(IFileService fileService, IEmployeeRepository repository, IUnitOfWork unitOfWork, IExcelExportService excelExportService)
        {
            _fileService = fileService;
            _repository = repository;
            _unitOfWork = unitOfWork;
            _excelExportService = excelExportService;
        }

        // GET: api/Employee
        //[HttpGet]
        //public OkObjectResult Get()
        //{
            
            
        //    return Ok("Hello");
        //}

        // GET: api/Employee/5
        [HttpGet("{id}", Name = "Get")]
        public async Task Get(int id)
        {
            var employee = await _repository.GetByIdAsync(id);

            await _excelExportService.Export(employee); 
        }

        // POST: api/Employee
        [HttpPost]
        public async Task Post(string fileName)
        {
            var employee = await _fileService.GetContentAsync(fileName);

            await _repository.InsertAsync(employee);

            await _unitOfWork.CompleteAsync();

        }

        // PUT: api/Employee/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
