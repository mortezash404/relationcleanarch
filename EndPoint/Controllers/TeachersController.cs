﻿using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using DAL.Repositories.Contracts;
using DAL.UnitOfWork;
using EndPoint.Dto.Teacher;
using Entities;

namespace EndPoint.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TeachersController : ControllerBase
    {
        #region Dependency Injection

        private readonly ITeacherRepository _repository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        #endregion

        #region Constructor
        public TeachersController(ITeacherRepository repository, IUnitOfWork unitOfWork, IMapper mapper)
        {
            _repository = repository;
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        } 
        #endregion

        // GET: api/Teachers
        //[HttpGet]
        //public async Task<ActionResult<IEnumerable<Teacher>>> GetTeachers()
        //{
        //    return await _context.Teachers.ToListAsync();
        //}

        // GET: api/Teachers/5
        [HttpGet("{id}")]
        public async Task<ActionResult<OutputTeacher>> GetTeacher(int id)
        {
            var teacher = await _repository.GetByIdAsync(id);

            if (teacher == null)
            {
                return NotFound();
            }

            return _mapper.Map<OutputTeacher>(teacher);
        }

        // PUT: api/Teachers/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        //[HttpPut("{id}")]
        //public async Task<IActionResult> PutTeacher(int id, Teacher teacher)
        //{
        //    if (id != teacher.Id)
        //    {
        //        return BadRequest();
        //    }

        //    _context.Entry(teacher).State = EntityState.Modified;

        //    try
        //    {
        //        await _context.SaveChangesAsync();
        //    }
        //    catch (DbUpdateConcurrencyException)
        //    {
        //        if (!TeacherExists(id))
        //        {
        //            return NotFound();
        //        }
        //        else
        //        {
        //            throw;
        //        }
        //    }

        //    return NoContent();
        //}

        // POST: api/Teachers
        
        [HttpPost]
        public async Task<ActionResult<OutputTeacher>> PostTeacher(CreateTeacher input)
        {
            var teacher = _mapper.Map<Teacher>(input);

            await _repository.InsertAsync(teacher);

            await _unitOfWork.CompleteAsync();

            return Ok(_mapper.Map<OutputTeacher>(teacher));
        }

        // DELETE: api/Teachers/5
        //[HttpDelete("{id}")]
        //public async Task<ActionResult<Teacher>> DeleteTeacher(int id)
        //{
        //    var teacher = await _context.Teachers.FindAsync(id);
        //    if (teacher == null)
        //    {
        //        return NotFound();
        //    }

        //    _context.Teachers.Remove(teacher);
        //    await _context.SaveChangesAsync();

        //    return teacher;
        //}

        //private bool TeacherExists(int id)
        //{
        //    return _context.Teachers.Any(e => e.Id == id);
        //}
    }
}
