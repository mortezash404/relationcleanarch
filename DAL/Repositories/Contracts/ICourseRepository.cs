﻿using System.Threading.Tasks;
using Entities;

namespace DAL.Repositories.Contracts
{
    public interface ICourseRepository
    {
        Task<Course> GetByIdAsync(int id);

        Task InsertAsync(Course course);
    }
}
