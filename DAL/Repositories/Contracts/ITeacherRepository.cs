﻿using System.Threading.Tasks;
using Entities;

namespace DAL.Repositories.Contracts
{
    public interface ITeacherRepository
    {
        Task<Teacher> GetByIdAsync(int id);

        Task InsertAsync(Teacher teacher);
    }
}
