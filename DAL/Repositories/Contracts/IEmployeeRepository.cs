﻿using System.Threading.Tasks;
using Entities;

namespace DAL.Repositories.Contracts
{
    public interface IEmployeeRepository
    {
        Task<Employee> GetByIdAsync(int id);
        Task InsertAsync(Employee employee);
    }
}