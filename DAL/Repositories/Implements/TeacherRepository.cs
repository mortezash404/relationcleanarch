﻿using System.Threading.Tasks;
using DAL.Repositories.Contracts;
using Entities;

namespace DAL.Repositories.Implements
{
    public class TeacherRepository : ITeacherRepository
    {
        private readonly AppDbContext _context;

        public TeacherRepository(AppDbContext context)
        {
            _context = context;
        }

        public async Task<Teacher> GetByIdAsync(int id)
        {
            return await _context.Teachers.FindAsync(id);
        }

        public async Task InsertAsync(Teacher teacher)
        {
            await _context.Teachers.AddAsync(teacher);
        }
    }
}
