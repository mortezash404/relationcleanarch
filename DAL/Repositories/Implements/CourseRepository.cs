﻿using System.Threading.Tasks;
using DAL.Repositories.Contracts;
using Entities;

namespace DAL.Repositories.Implements
{
    public class CourseRepository : ICourseRepository
    {
        private readonly AppDbContext _context;

        public CourseRepository(AppDbContext context)
        {
            _context = context;
        }

        public async Task<Course> GetByIdAsync(int id)
        {
            return await _context.Courses.FindAsync(id);
        }

        public async Task InsertAsync(Course course)
        {
            await _context.Courses.AddAsync(course);
        }
    }
}
