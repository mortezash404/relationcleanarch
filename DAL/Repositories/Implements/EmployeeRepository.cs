﻿using System.Threading.Tasks;
using DAL.Repositories.Contracts;
using Entities;

namespace DAL.Repositories.Implements
{
    public class EmployeeRepository : IEmployeeRepository
    {
        private readonly AppDbContext _context;

        public EmployeeRepository(AppDbContext context)
        {
            _context = context;
        }

        public async Task<Employee> GetByIdAsync(int id)
        {
            return await _context.Employees.FindAsync(id);
        }

        public async Task InsertAsync(Employee employee)
        {
            await _context.Employees.AddAsync(employee);
        }
    }
}
