﻿namespace Entities
{
    public class Course
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int Unit { get; set; }

        // navigation properties
        public int? TeacherId { get; set; }

        public Teacher Teacher { get; set; }
    }
}
