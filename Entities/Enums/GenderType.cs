﻿namespace Entities.Enums
{
    public enum GenderType
    {
        Male,
        Female
    }
}