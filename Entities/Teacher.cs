﻿using System.Collections.Generic;
using Entities.Enums;

namespace Entities
{
    public class Teacher
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public DegreeType DegreeType { get; set; }

        public int Age { get; set; }

        // navigation property

        public ICollection<Course> Courses { get; set; } = new HashSet<Course>();
    }
}
