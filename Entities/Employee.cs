﻿using Entities.Enums;

namespace Entities
{
    public class Employee
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Age { get; set; }
        public GenderType GenderType { get; set; }
        public string Phone { get; set; }
    }
}
